'''
This problem was asked by Uber.
Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
Follow-up: what if you can’t use division?
'''

list_ = [1,2,4,6,8]
leng = len(list_)
right = [1]
left = [1]
for i, number in enumerate(list_):
    j = leng - i - 1
    if i == 0:
        left.append(list_[i])
        right.insert(0, list_[j])
    elif i < leng - 1 :
        left.append(list_[i] * left[-1])
        right.insert(0, right[0] * list_[j])
print(right)
print(left)
result = []
for i in range(leng):
    result.append(left[i] * right[i])
print(result)
