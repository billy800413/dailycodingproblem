'''
This problem was asked by Facebook.
Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.
For example, the message ‘111’ would give 3, since it could be decoded as ‘aaa’, ‘ka’, and ‘ak’.
You can assume that the messages are decodable. For example, ‘001’ is not allowed.
'''

a2z = [chr(w) for w in range(97, 123)]
mapp = {}
for i, ch in enumerate(a2z):
    mapp[i+1] = ch


count = 0
result = []
# preStr : finish, a~z
# str_ : unfinish, still number
class myClass():
    def __init__(self, str_, preStr):
        self.str_ = str_
        self.path1 = None
        self.path2 = None
        global count
        global result
        len_ = len(str_)
        self.preStr = preStr
        if len_ == 0:
            count += 1
            result.append(preStr)
        if len_ > 0 and int(self.str_[0]) > 0:
            ch = mapp[int(self.str_[0])]
            self.path1 = myClass(self.str_[1:], self.preStr + ch)
        if len_ > 1 and int(self.str_[:2]) <= 26 and int(self.str_[:2]) >= 10:
            ch = mapp[int(self.str_[:2])]
            self.path2 = myClass(self.str_[2:], self.preStr + ch)




myClass("11961", '')
print(count)
print(result)
