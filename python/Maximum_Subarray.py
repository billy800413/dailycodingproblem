'''
Given an integer array nums, find the contiguous subarray 
(containing at least one number) which has the largest sum and return its sum.

Example:

Input: [-2,1,-3,4,-1,2,1,-5,4],
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.

Follow up:

If you have figured out the O(n) solution, try coding 
another solution using the divide and conquer approach, which is more subtle.
'''

class Solution:
    def maxSubArray(self, nums: list) -> int:
        leng = len(nums)
        currentmax = nums[0]
        globalmax = nums[0]
        for index,value in  enumerate(nums[1:]):
            print(value)
            if currentmax + value > value:
                currentmax += value
            else:
                currentmax = value
            if currentmax > globalmax:
                globalmax = currentmax
        return globalmax

myclass = Solution()
print(myclass.maxSubArray([-1, 2, 3 , -5, 4]))