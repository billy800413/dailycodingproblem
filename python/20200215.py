# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
# Given the root to a binary tree, implement serialize(root), 
# which serializes the tree into a string, and deserialize(s), 
# which deserializes the string back into the tree.

class TreeNode(object):
     def __init__(self, x):
         self.val = x
         self.left = None
         self.right = None

class Codec:

    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        if root == None:
            return "null"
        leftSerialize = self.serialize(root.left)
        rightSerialize = self.serialize(root.right)
        return root.val + "," + leftSerialize + "," + rightSerialize
        
        

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        def deserializeHelp(list_):
            if len(list_) > 0:
                ValueForNode = list_.pop(0)
                if ValueForNode == "null":
                    return None
                newNode = TreeNode(ValueForNode)
                if len(list_) > 0:
                    newNode.left = deserializeHelp(list_)
                    newNode.right = deserializeHelp(list_)
                return newNode
        list_ = data.split(",")
        return deserializeHelp(list_)




codec = Codec()
de = codec.deserialize('1,1,1,null,null,1,null,null,2,null,null')
en = codec.serialize(de)
print(en)

