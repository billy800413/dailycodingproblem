'''
cons(a, b) constructs a pair, and car(pair) and cdr(pair) 
returns the first and last element of that pair. 
For example, car(cons(3, 4)) returns 3, 
and cdr(cons(3, 4)) returns 4.
'''

# Given this implementation of cons:
def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair

def car(pair):
    my_func = lambda a, b: a
    return pair(my_func)

def cdr(pair):
    my_func = lambda a, b: b
    return pair(my_func)

my_pair = cons(1, 2)
print(car(my_pair))
print(cdr(my_pair))