'''
Given two strings s and t , write a function to determine if t is an anagram of s.

Input: s = "anagram", t = "nagaram"
Output: true

Input: s = "rat", t = "car"
Output: false
'''

class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        dict_ = {}
        if len(s) != len(t):
            return False
        for ch in t:
            if ch not in dict_:
                dict_[ch] = 1
            else:
                dict_[ch] += 1
        for ch in s:
            if ch in dict_:
                dict_[ch] -= 1
                if dict_[ch] < 0:
                    return False
            else:
                return False
        return True