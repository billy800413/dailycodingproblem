class Solution:
    def countAndSay(self, n: int) -> str:
        def fun(n, str_=""):
            result = ""
            if n == 1:
                return "1"
            elif n >= 2:
                list_ = []
                x = 0
                y = 0
                while 1:
                    if y < len(str_):
                        if x == y:
                            list_.append([str_[x], 1])
                            y += 1
                        elif str_[x] == str_[y]:
                            y += 1
                            list_[-1][1] += 1
                        elif str_[x] != str_[y]:
                            x = y
                    else:
                        break
                for num, f in list_:
                    result += (str(f) + num) 
                return result
        result = ''
        for i in range(1, n+1, 1):
            result = fun(i, result)
        return result