'''
Given a string, find the first non-repeating character in it and 
return it's index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Note: You may assume the string contain only lowercase letters.


'''
class Solution:
    def firstUniqChar(self, s):
        leng =len(s)
        set_ = set()
        for i in range(leng):
            if s[i] not in set_:
                tmp = s[:i] + s[i+1:]
                if s[i] not in tmp:
                    return i
                set_.add(s[i])
        return -1

