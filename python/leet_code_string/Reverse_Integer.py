'''
Given a 32-bit signed integer, reverse digits of an integer.
'''
class Solution:
    def reverse(self, x):
        # -2^31 and 2^31-1
        if x >= 0:
            tmp = int(str(x)[::-1])
            if len(bin(tmp)[2:]) <=31:
                return tmp
            else:
                return 0
        else:
            tmp = int(str(x)[0]+str(x)[1:][::-1])
            if len(bin(tmp)[3:]) <=31:
                return tmp
            else:
                return 0