'''
Implement atoi which converts a string to an integer.

The function first discards as many whitespace characters as necessary 
until the first non-whitespace character is found. Then, starting from 
this character, takes an optional initial plus or minus sign followed 
by as many numerical digits as possible, and interprets them as a numerical value.

The string can contain additional characters after those that form 
the integral number, which are ignored and have no effect on the behavior 
of this function.

If the first sequence of non-whitespace characters in str is not a valid 
integral number, or if no such sequence exists because either str is empty 
or it contains only whitespace characters, no conversion is performed.

If no valid conversion could be performed, a zero value is returned.

Note:

Only the space character ' ' is considered as whitespace character.
Assume we are dealing with an environment which could only store integers 
within the 32-bit signed integer range: [−231,  231 − 1]. If the numerical 
value is out of the range of representable values, INT_MAX (231 − 1) or 
INT_MIN (−231) is returned.
'''

class Solution:
    def myAtoi(self, str: str) -> int:
        str = str.strip()
        if len(str) == 0:
            return 0
        str2 = ''
        if str[0] in "-+" and len(str)>1:
            for ch in str[1:]:
                if ch in "0123456789":
                    str2 += ch
                else:
                    break
            if len(str2) == 0:
                return 0
            str2 = str[0] + str2
        elif str[0] in "0123456789":
            for ch in str:
                if ch in "0123456789":
                    str2 += ch
                else:
                    break
        else:
            return 0
        num = int(str2)
        if num < 0 and len(bin(num)[3:]) > 31:
            return -2147483648
        elif num >= 0 and len(bin(num)[2:]) > 31:
            return 2147483647
        else:
            return num

myclass = Solution()
print(myclass.myAtoi("+-1"))