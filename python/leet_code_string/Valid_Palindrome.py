'''
Given a string, determine if it is a palindrome, considering 
only alphanumeric characters and ignoring cases.

Note: For the purpose of this problem, we define empty string as valid palindrome.
'''

class Solution:
    def isPalindrome(self, s: str) -> bool:
        if len(s) == 0:
            return True
        s2 = ''
        for ch in s:
            chLower = ch.lower()
            if (ord(chLower) <= 122 and ord(chLower) >= 97) or  (ord(chLower) <= 57 and ord(chLower) >= 48):
                s2 += chLower
        if s2[::-1] != s2:
            return False
        return True

class Solution2:
    def isPalindrome(self, s: str) -> bool:
        if len(s) == 0:
            return True
        leng = len(s)
        x, y = 0, leng -1
        s = s.lower()
        for _ in range(leng):
            while 1:
                if y == 0:
                    break
                if (ord(s[y]) <= 122 and ord(s[y]) >= 97) or  (ord(s[y]) <= 57 and ord(s[y]) >= 48): 
                    break
                else:
                    y -= 1
            while 1:
                if x == leng -1:
                    break
                if (ord(s[x]) <= 122 and ord(s[x]) >= 97) or  (ord(s[x]) <= 57 and ord(s[x]) >= 48):
                    break
                else:
                    x += 1
            if x >= y:
                return True
            elif s[x] != s[y]:
                return False
            elif s[x] == s[y]:
                y -= 1
                x += 1

class Solution3:
    def isPalindrome(self, s: str) -> bool:
        s = "".join(list(filter(str.isalnum, s.lower())))
        if s[::-1] == s:
            return True
        else:
            return False

input_ = "."
myclass = Solution3()
print(myclass.isPalindrome(input_))