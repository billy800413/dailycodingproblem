'''
Return the index of the first occurrence of needle in haystack, 
or -1 if needle is not part of haystack.

Example 1:

Input: haystack = "hello", needle = "ll"
Output: 2
Example 2:

Input: haystack = "aaaaa", needle = "bba"
Output: -1
'''

class Solution:
    def strStr(self, haystack: str, needle: str) -> int:
        leng = len(needle)
        leng2 = len(haystack)
        if haystack == needle:
            return 0
        for i in range(leng2):
            if (leng2 - i) >= leng:
                if haystack[i:].startswith(needle):
                    return i
            else:
                return -1
        return -1

myclass = Solution()
print(myclass.strStr("hello", "ll"))