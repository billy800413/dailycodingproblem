# Definition for a binary tree node.
'''
Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

Note: A leaf is a node with no children.
'''

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def maxDepth(self, root: TreeNode) -> int:
        deep = 1
        deepList = []
        if root == None:
            return 0
        class myclass():
            def getMoreDeep(Node:TreeNode, DeepNow, deepList):
                if Node.left != None:
                    myclass.getMoreDeep(Node.left, DeepNow + 1, deepList)
                else:
                    deepList.append(DeepNow)
                if Node.right != None:
                    myclass.getMoreDeep(Node.right, DeepNow + 1, deepList)
                else:
                    deepList.append(DeepNow)
        myclass.getMoreDeep(root, deep, deepList)
        return max(deepList)