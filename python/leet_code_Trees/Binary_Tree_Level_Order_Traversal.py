'''
Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
'''

# Definition for a binary tree node.
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:
        deepDict = dict()
        # deepList
        deep = 0
        if root == None:
            return []
        def check(root, deepDict, deep):
            if root != None:
                if deep not in deepDict:
                    deepDict[deep] = [root.val]
                else:
                    deepDict[deep].append(root.val)
                check(root.left, deepDict, deep + 1)
                check(root.right, deepDict, deep + 1)
        check(root, deepDict, deep)
        result = []
        for i in range(len(deepDict)):
            result.append(deepDict[i])
        return result