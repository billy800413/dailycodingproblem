'''
There exists a staircase with N steps, and you can 
climb up either 1 or 2 steps at a time. Given N, write a function that returns the number of unique ways you can climb the staircase. The order of the steps matters.
For example, if N is 4, then there are 5 unique ways:
1, 1, 1, 1
2, 1, 1
1, 2, 1
1, 1, 2
2, 2
'''
ways = []
N = 4
def fun(stair:int, way:str, N:int, ways:list):
    if N - stair > 1:
        fun(stair+1, way + '1', N, ways)
        fun(stair+2, way + '2', N, ways)
    elif N - stair == 1:
        fun(stair+1, way + '1', N, ways)
    else:
        ways.append(way)
fun(0, "", 4, ways)
print(ways)
