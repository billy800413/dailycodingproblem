
'''
Given an integer k and a string s, find the length of the longest 
substring that contains at most k distinct characters.

For example, given s = "abcba" and k = 2, the longest substring with k distinct characters is "bcb".
'''

def fun(input_: str, k: int) ->str:
    unfinish_ = []
    preCh = ""
    for ch in input_:
        if ch != preCh:
            preCh = ch
            unfinish_.append(["", set(), False])
        for Candidate in unfinish_:
            if not Candidate[2]:
                if ch in Candidate[1]:
                    Candidate[0] += ch
                elif ch not in Candidate[1] and len(Candidate[1]) < k:
                    Candidate[0] += ch
                    Candidate[1].add(ch)
                elif ch not in Candidate[1] and len(Candidate[1]) == k:
                    Candidate[2] = True
    unfinish_ = sorted(unfinish_, key = lambda Candidate : len(Candidate[0]))
    return unfinish_[-1][0]

print(fun("abcbab", 3))


