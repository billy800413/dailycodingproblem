class ListNode:
    def __init__(self, x):
         self.val = x
         self.next = None

def add(Node, val):
    while Node.next != None:
        Node = Node.next
    Node.next = ListNode(val)
    return 0

list_ = [1,2,3,4,5]
nodes = ListNode(list_[0])
for i in list_[1:]:
    add(nodes, i)

while nodes.next != None:
    print(nodes.val)
    nodes = nodes.next
print(nodes.val)
