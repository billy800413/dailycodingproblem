# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def reverseList(self, head: ListNode) -> ListNode: 
        if head == None:
            return head
        elif head.next == None:
            return head

        leng = 0
        node = head
        valList = []
        while node != None:
            valList.append(node.val)
            node = node.next
            leng += 1
        valList[:] = valList[::-1]
        
        def add(Node, val):
            while Node.next != None:
                Node = Node.next
            Node.next = ListNode(val)
            return 0
        
        newhead = ListNode(valList[0])
        for i in valList[1:]:
            add(newhead, i)
        return newhead

class Solution2:
    def reverseList(self, head: ListNode) -> ListNode: 
        if head == None or head.next == None:
            return head
        cur = head
        pre = None
        while cur != None:
            next_ = cur.next
            # keep fisrt, change next connection
            cur.next = pre
            # go to its head
            pre = cur
            cur = next_
        return pre


class Solution3:
    def reverseList(self, head: ListNode) -> ListNode: 
        if head == None or head.next == None:
            return head
        p = self.reverseList(head.next)
        head.next.next = head
        head.next = None
        return p