# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def hasCycle(self, head: ListNode) -> bool:
        nodeSet = set()
        if head == None:
            return False
        while 1:
            nodeSet.add(head)
            head = head.next
            if head in nodeSet:
                return True
            if head == None:
                return False