'''
Input: o1->o2->o3, k1->k2->k3
Output: o1->k1->o2->k2->o3->k3
'''

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def mergeTwoLists(self, l1: ListNode, l2: ListNode) -> ListNode:
        if l1 == None:
            return l2
        elif l2 == None:
            return l1
        new  = ListNode(0)
        head = new
        while 1:
            l1_next = l1.next
            l2_next = l2.next
            if l1_next != None and l2_next != None:
                new.next = ListNode(l1.val)
                new = new.next
                new.next = ListNode(l2.val)
                new = new.next
                l1 = l1_next
                l2 = l2_next
            else:
                new.next = ListNode(l1.val)
                new = new.next
                new.next = ListNode(l2.val)
                new = new.next
                break
        if l1_next != None:
            new.next = l1_next
        if l2_next != None:
            new.next = l2_next
            
        return head.next