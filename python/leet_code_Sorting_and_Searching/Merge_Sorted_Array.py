'''
Given two sorted integer arrays nums1 and nums2, merge nums2 into 
nums1 as one sorted array.

Note:

The number of elements initialized in nums1 and nums2 are m and n respectively.
You may assume that nums1 has enough space (size that is greater or 
equal to m + n) to hold additional elements from nums2.

Example:

Input:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

Output: [1,2,2,3,5,6]
'''

class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        if len(nums1) == 1:
            if len(nums2) > 0:
                nums1[0] = nums2.pop(0)
            return 0
        for _ in range(len(nums2)):
            nums1.pop(-1)

        count = 0
        while 1:
            if len(nums2) == 0:
                break
            num = nums2[0]
            if len(nums1) == 0:
                nums1.insert(0, num)
                nums2.pop(0)
            elif nums1[count] >= num:
                nums1.insert(count, num)
                nums2.pop(0)
            elif nums1[count] < num:
                if count + 1 < len(nums1) and nums1[count+1] < num:
                    count += 1
                else:
                    nums2.pop(0)
                    nums1.insert(count+1, num)
                    count += 1
            else:
                count += 1





l1 = [-1,0,0,3,3,3,0,0,0]
l2 = [1,2,2]
merge(l1, l2)
print(l1)