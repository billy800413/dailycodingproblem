'''
Given an array of integers, find if the array contains any duplicates.
Your function should return true if any value appears at least twice in 
the array, and it should return false if every element is distinct.
'''

class Solution:
    def containsDuplicate(self, nums):
        for i in nums:
            if nums.count(i) > 1:
                return True
        return False

class Solution2:
    def containsDuplicate(self, nums):
        for i in range(len(nums)-1):
            if nums[i] in nums[i+1:]:
                return True
        return False

class Solution3:
    def containsDuplicate(self, nums):
        temp = set()
        for value in nums:
            if value not in temp:
                temp.add(value)
            else:
                return True
        return False

myclass = Solution3()
print(myclass.containsDuplicate([1,3]))