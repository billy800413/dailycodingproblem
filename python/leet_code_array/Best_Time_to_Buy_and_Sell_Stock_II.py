'''
Say you have an array for which the ith element 
is the price of a given stock on day i.
Design an algorithm to find the maximum profit. You may complete as many 
transactions as you like (i.e., buy one and sell one share of the stock multiple times).
'''
class Solution:
    def maxProfit(self, prices):
        class myclass():
            def __init__(self, RemainingPrices, previousBuy, totalEarn, totalEarnList):
                self.RemainingPrices = RemainingPrices
                self.previousBuy = previousBuy
                self.totalEarn = totalEarn
                self.next = []
                if len(self.RemainingPrices) > 0:
                    if previousBuy is not None:
                        for idx in range(len(self.RemainingPrices)):
                            if self.previousBuy < self.RemainingPrices[idx]:
                                self.next.append(
                                    myclass(
                                        RemainingPrices[idx+1:],
                                        None,
                                        self.totalEarn + (self.RemainingPrices[idx] - self.previousBuy),
                                        totalEarnList
                                ))
                    else:
                        for idx in range(len(self.RemainingPrices)):
                            self.next.append(
                                    myclass(
                                        RemainingPrices[idx+1:],
                                        RemainingPrices[idx],
                                        self.totalEarn,
                                        totalEarnList,
                                ))
                else:
                    totalEarnList.append(self.totalEarn)

        totalEarnList = []
        a = myclass(prices, None, 0, totalEarnList)
        return max(totalEarnList)


class Solution2:
    def maxProfit(self, prices):
        earn = 0
        buyNow = None
        for idx in range(len(prices)):
            if buyNow == None and idx <= len(prices) - 2:
                if prices[idx+1] - prices[idx] > 0:
                    buyNow = prices[idx]
            elif buyNow != None and prices[idx] - buyNow > 0:
                earn += (prices[idx] - buyNow)
                buyNow = None
                if idx <= len(prices) - 2: # also buy again
                    if prices[idx+1] - prices[idx] > 0:
                        buyNow = prices[idx]
        return earn

            






myclass = Solution2()
print(myclass.maxProfit([1,2,3,4,5]))
