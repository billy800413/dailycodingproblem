'''
Determine if a 9x9 Sudoku board is valid. Only the filled cells 
need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.
'''

class Solution:
    def isValidSudoku(self, board):
        list_ = []
        # check row
        for i in range(9):
            list_.append(board[i])
        # check column
        for i in range(9):
            temp = []
            for j in range(9):
                temp.append(board[j][i])
            list_.append(temp)
        # check subbox
        for i in range(0, 9, 3):
            for j in range(0, 9, 3):
                temp = []
                for m in range(3):
                    for n in range(3):
                        temp.append(board[i+m][j+n])
                list_.append(temp)
        if False in map(self.check, list_):
            return False
        else:
            return True
        
    def check(self, numberList):
            set_ = set()
            for number in numberList:
                if number != '.':
                    if number in set_:
                        return False
                    else:
                        set_.add(number)
            return True

myclass = Solution()
input_ = [
    ["8","3",".",  ".","7",".",  ".",".","."],
    ["6",".",".",  "1","9","5",  ".",".","."],
    [".","9","8",  ".",".",".",  ".","6","."],

    ["8",".",".",  ".","6",".",  ".",".","3"],
    ["4",".",".",  "8",".","3",  ".",".","1"],
    ["7",".",".",  ".","2",".",  ".",".","6"],

    [".","6",".",  ".",".",".",  "2","8","."],
    [".",".",".",  "4","1","9",  ".",".","5"],
    [".",".",".",  ".","8",".",  ".","7","9"]]

print(myclass.isValidSudoku(input_))