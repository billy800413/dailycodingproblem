class Solution:
    def rotate(self, matrix):
        """
        Do not return anything, modify matrix in-place instead.
        """
        leng = len(matrix)
        for i in range(leng):
            for j in range(i, leng, 1):
                matrix[j][i] , matrix[i][j] = matrix[i][j], matrix[j][i]

        for i in range(len(matrix)):
            matrix[i] = matrix[i][::-1]
        

input_ = [
            [1,2,3],
            [4,5,6],
            [7,8,9]
        ]
myclass = Solution()
myclass.rotate(input_)
print(input_)