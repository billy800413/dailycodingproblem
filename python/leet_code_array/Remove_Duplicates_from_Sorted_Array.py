'''
Given a sorted array nums, remove the duplicates in-place such that 
each element appear only once and return the new length.
Do not allocate extra space for another array, you must do 
this by modifying the input array in-place with O(1) extra memory.
'''

class Solution:
    def removeDuplicates(self, nums):
        for i in nums:
            print(i)
            print(nums)
            for _ in range(nums.count(i)-1):
                nums.remove(i)
            print(nums)
        return len(nums)

class Solution2:
    def removeDuplicates(self, nums):
        print(0, nums)
        x = 0
        y = 1
        while y < len(nums):
            if nums[x] == nums[y]:
                y += 1
                print(1, nums)
            else:
                x += 1
                nums[x] = nums[y]
                y += 1
                print(2, nums)
        return x + 1

myclass = Solution()
print(myclass.removeDuplicates([1,1,2,2,2,3]))

