'''
Given a non-empty array of integers, every element appears twice except for one. 
Find that single one.

Note:
Your algorithm should have a linear runtime complexity.
Could you implement it without using extra memory?
'''

class Solution:
    def singleNumber(self, nums):
        nums.sort()
        if len(nums) == 1 or nums[0] != nums[1]:
            return nums[0]
        for i in range(1,len(nums)-1, 1):
            if nums[i-1] != nums[i] and nums[i+1] != nums[i]:
                return nums[i]
        return nums[-1]



myclass = Solution()
print(myclass.singleNumber([1,2,2,2]))