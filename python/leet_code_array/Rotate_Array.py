'''
Given an array, rotate the array to the right by k steps, where k is non-negative.
Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
Could you do it in-place with O(1) extra space?
'''

class Solution:
    def rotate(self, nums, k):
        """
        Do not return anything, modify nums in-place instead.
        """
        k = k % len(nums) # k step be a loop
        if k == 0:
            pass
        else:
            nums[:] = nums[-k:] + nums[:-k]

        return None

myclass = Solution()
list_ = [1,2,3,]
k = 4
myclass.rotate(list_, k)
print(list_)