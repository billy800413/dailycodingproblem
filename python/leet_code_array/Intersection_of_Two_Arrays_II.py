'''
Given two arrays, write a function to compute their intersection.
Note:

Each element in the result should appear as many times as it shows in both arrays.
The result can be in any order.
Follow up:

What if the given array is already sorted? How would you optimize your algorithm?
What if nums1's size is small compared to nums2's size? Which algorithm is better?
What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
'''

class Solution:
    def intersect(self, nums1, nums2):
        if len(nums1) == 0 or len(nums2) == 0:
            return []
        temp = dict()
        for number in nums1:
            if number not in temp:
                temp[number] = 1
            else:
                temp[number] += 1
        result = []
        for number in nums2:
            if temp.get(number,0) > 0:
                result.append(number)
                temp[number] -= 1
        return result

