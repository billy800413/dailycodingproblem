'''
Given an array nums, write a function to move all 0's to the end of
it while maintaining the relative order of the non-zero elements.

Note:
You must do this in-place without making a copy of the array.
Minimize the total number of operations.
'''

class Solution:
    def moveZeroes(self, nums):
        """
        Do not return anything, modify nums in-place instead.
        """
        x = 0
        y = 1
        for _ in range(len(nums)):
            if nums[x] == 0:
                for _ in range(y ,len(nums), 1):
                    if nums[y] != 0:
                        nums[x], nums[y] = nums[y], nums[x]
                        x += 1
                        y += 1
                        break
                    else:
                        y += 1
            else:
                x += 1
                y += 1
        return None

list_ = [1,2,3,0,4,6]
myclass = Solution()
myclass.moveZeroes(list_)
print(list_)