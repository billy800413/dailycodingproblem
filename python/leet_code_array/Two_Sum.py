'''
Given an array of integers, return indices of the two numbers such 
that they add up to a specific target.
You may assume that each input would have exactly one solution, 
and you may not use the same element twice.
'''

class Solution:
    def twoSum(self, nums, target):
        dict_ = dict()
        for index, value in enumerate(nums):
            if target - value in dict_:
                return [dict_[target - value], index]
            else:
                dict_[value] = index